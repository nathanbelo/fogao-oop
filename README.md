# FOGÃO OOP

Este é um projeto Javascript com Orientação a Objeto

## Pré-requisitos

Certifique-se de ter o Docker e o Docker Compose instalados na sua máquina.

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Como Rodar

1. Clone o repositório:
```bash
   git clone https://gitlab.com/nathanbelo/fogao-oop
```

2. Navegue até o diretório do projeto:
```bash
   cd fogao-oop
```

3. Construa as imagens Docker:
```bash
   docker-compose up --build
```

4. Acesse a aplicação no navegador em [http://localhost:8003](http://localhost:8003).

5. Ao clicar nas bocas do fogão é exibido um alert informando qual boca foi acesa. O mesmo acontece para a luz do forno.

## Como parar

Para parar os containers, pressione Ctrl + C no terminal onde o docker-compose up está rodando.

