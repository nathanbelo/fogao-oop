import {Lighter} from "./lighter.js";
import {Oven} from "./oven.js";

export class Stove {
  constructor(color, dimensions, brand, ovenLight, lighter = null) {
    this.color = color;
    this.dimensions = dimensions;
    this.brand = brand;
    this.oven = new Oven(true, ovenLight, { glass: true, glassDimensions: "50x30" });
    if(lighter){
      this.lighters = new Lighter(lighter);
    }
  }

  stoveDescription(){
    alert(`It is a ${this.color} stove with dimensions: ${this.dimensions}. 
          The glass of oven has dimensions: ${this.oven.door.glassDimensions}. This stove brand is ${this.brand}`);
  }
}