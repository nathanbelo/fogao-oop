import { Stove } from "./stove.js";

window.turnOnBurner = function(lighter) {
    let myStove = new Stove("black", '100x150x120', "Brastemp", null, lighter);
    myStove.lighters.turnOn()
}

window.turnOnLamp = function() {
    let myStove = new Stove("black", '100x150x120', "Brastemp", true);
    myStove.oven.turnOnLamp()
}

window.turnOffLamp = function() {
    let myStove = new Stove("black", '100x150x120', "Brastemp", false);
    myStove.oven.turnOffLamp()
}

window.stoveDescription = function() {
    let myStove = new Stove("black", '100x150x120', "Brastemp", false);
    myStove.stoveDescription();
}