export class Oven {
    constructor(lamp, lampButton, door) {
      this.lamp = lamp;
      this.lampButton = lampButton;
      this.door = door;
    }
  
    turnOnLamp() {
      this.lampButton = true;
      var element = document.querySelector('.oven-glass');
      element.style.backgroundColor = 'yellow';      
    }
  
    turnOffLamp() {
      this.lampButton = false;
      var element = document.querySelector('.oven-glass');
      element.style.backgroundColor = 'lightblue';
    }
  }
